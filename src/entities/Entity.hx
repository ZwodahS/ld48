package entities;

class Entity extends h2d.Object {
	public var canBreak: Bool = false;
	public var absorb: Float = 0;
	public var bounce: Float = 0;
	public var velocity: Point2f;
	public var terminal: Point2f;
	public var movement: Point2f;
	public var stable: Bool = false;
	public var collision: Bool = true;

	public function new() {
		super();
		this.velocity = [0, 0];
		this.terminal = [0, 300];
		this.movement = [0, 0];
	}

	public var bound(get, set): Rectf;

	public function get_bound(): Rectf {
		return [this.x + 0, this.y + 0, this.x + 7, this.y + 7];
	}

	public function set_bound(r: Rectf): Rectf {
		this.x = r.x;
		this.y = r.y;
		return this.get_bound();
	}

	public function onCollide(e: Entity, gs: GameScreen) {}

	public function update(dt: Float) {
		if (!this.stable) {
			if (this.velocity.y <= this.terminal.y) {
				this.velocity.y += Constants.AccelerationY * dt;
			}
			if (this.velocity.y > this.terminal.y) {
				// correct it towards terminal
				this.velocity.y = Math.clampF(this.velocity.y - (dt * Constants.Drag), this.terminal.y, null);
			}
			this.movement = [this.velocity.x * dt, this.velocity.y * dt];
			this.x += this.movement.x;
			this.y += this.movement.y;
			this.x = Math.clampF(this.x, 0, Globals.game.boundedSize.x - 8);
		}
	}

	public function onCollidePlayer(player: Entity, i: IntersectDetail, gs: GameScreen) {
		if (this.collision) {
			if (i.y < 4) {
				if (i.yType == Negative) {
					player.y -= i.y;
					var vy = player.velocity.y;
					if (this.absorb > 0) vy = Math.decreaseAbsoluteValue(vy, this.absorb);
					vy = -vy / 2;
					vy -= this.bounce;
					player.velocity.y = vy;
				} else if (i.yType == Positive) {
					player.y += i.y;
					var vy = player.velocity.y;
					if (this.absorb > 0) vy = Math.decreaseAbsoluteValue(vy, this.absorb);
					vy = -vy / 2;
					vy += this.bounce;
					player.velocity.y = vy;
				} else {}
			} else {
				if (i.xType == Negative) {
					player.x -= i.x;
					player.velocity.x = -100;
				} else {
					player.x += i.x;
					player.velocity.x = 100;
				}
			}
		}
	}
}
