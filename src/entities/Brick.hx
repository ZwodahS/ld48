package entities;

class Brick extends Entity {
	public static function make(): Entity {
		var e = new Brick();
		e.addChild(Assets.packed.assets["brick"].getBitmap());
		e.stable = true;
		e.absorb = 30;
		e.bounce = 40;
		e.canBreak = true;
		return e;
	}

	override public function onCollidePlayer(player: Entity, i: IntersectDetail, gs: GameScreen) {
		if (player.velocity.y >= 150) {
			gs.destroyEntity(this);
			gs.explodeEntity(this, Constants.ColorWhite);
		}
		if (player.velocity.y <= player.terminal.y - this.absorb) return super.onCollidePlayer(player, i, gs);
		player.velocity.y -= this.absorb;
	}
}
