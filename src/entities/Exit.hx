package entities;

class Exit extends Entity {
	public static function make(): Entity {
		var e = new Exit();
		e.stable = true;
		var bm = Assets.packed.assets["white_bg"].getBitmap();
		bm.color.setColor(Constants.ColorGreen);
		bm.alpha = .8;
		bm.height = 16;
		bm.width = 140;
		e.addChild(bm);
		return e;
	}

	override public function onCollide(e: Entity, gs: GameScreen) {
		if (e != gs.player) return;
		gs.victory();
		gs.destroyEntity(this);
	}

	override public function get_bound(): Rectf {
		return [this.x + 0, this.y + 0, this.x + 140, this.y + 16];
	}
}
