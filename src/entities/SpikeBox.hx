package entities;

import zf.animations.*;

class SpikeBox extends Entity {
	var obj: h2d.Object;
	var animation: Animation;

	public static function make(): Entity {
		var e = new SpikeBox();
		var obj = new h2d.Object();
		obj.x = 4;
		obj.y = 4;
		var bm = Assets.packed.assets["box"].getBitmap();
		bm.x = -4;
		bm.y = -4;
		obj.addChild(bm);
		e.addChild(obj);
		e.stable = true;
		e.obj = obj;

		var spikeAbove = Assets.packed.assets["spike"].getBitmap(0);
		spikeAbove.x = -4;
		spikeAbove.y = -12;
		obj.addChild(spikeAbove);

		var spikeRight = Assets.packed.assets["spike"].getBitmap(1);
		spikeRight.x = 4;
		spikeRight.y = -4;
		obj.addChild(spikeRight);

		var spikeLeft = Assets.packed.assets["spike"].getBitmap(2);
		spikeLeft.x = -12;
		spikeLeft.y = -4;
		obj.addChild(spikeLeft);

		var spikeBelow = Assets.packed.assets["spike"].getBitmap(3);
		spikeBelow.x = -4;
		spikeBelow.y = 4;
		obj.addChild(spikeBelow);
		return e;
	}

	override public function onCollidePlayer(player: Entity, i: IntersectDetail, gs: GameScreen) {
		if (player.velocity.y > 300) {
			gs.destroyEntity(this);
			gs.explodeEntity(this, Constants.ColorRed);
			super.onCollidePlayer(player, i, gs);
		} else {
			gs.explodePlayer();
		}
	}
}
