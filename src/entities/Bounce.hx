package entities;

import zf.animations.*;

class Bounce extends Entity {
	var obj: h2d.Object;
	var animation: Animation;

	public static function make(): Entity {
		var e = new Bounce();
		var obj = new h2d.Object();
		obj.x = 4;
		obj.y = 4;
		var bm = Assets.packed.assets["bounce"].getBitmap();
		bm.x = -4;
		bm.y = -4;
		obj.addChild(bm);
		e.addChild(obj);
		e.stable = true;
		e.bounce = 45;
		e.obj = obj;
		return e;
	}

	override public function onCollide(e: Entity, gs: GameScreen) {
		if (this.animation != null) return;
		this.animation = gs.animator.runAnim(new ScaleTo(new WrappedObject(this.obj), [1.4, 1.4], null, 3),
			function() {
				this.animation = null;
				this.obj.scaleX = 1.0;
				this.obj.scaleY = 1.0;
			});
	}
}
