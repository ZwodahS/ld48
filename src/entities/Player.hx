package entities;

import zf.animations.*;

class Player extends Entity {
	var obj: h2d.Object;
	var animation: Animation;

	public static function make(): Entity {
		var p = new Player();
		var obj = new h2d.Object();
		obj.x = 4;
		obj.y = 4;
		var bm = Assets.packed.assets["player"].getBitmap();
		bm.x = -4;
		bm.y = -4;
		obj.addChild(bm);
		p.obj = obj;
		p.addChild(obj);
		p.stable = false;
		return p;
	}

	override public function update(dt: Float) {
		super.update(dt);
		if (this.animation == null) {
			if (this.velocity.y > 0) {
				this.obj.scaleX = 1.0 - (this.velocity.y / this.terminal.y) * 0.1;
				this.obj.scaleY = 1.0 + (this.velocity.y / this.terminal.y) * 0.2;
			} else {
				this.obj.scaleX = 1.0;
				this.obj.scaleY = 1.0;
			}
		}
	}

	override public function onCollide(e: Entity, gs: GameScreen) {
		if (this.animation != null) return;
		if (Math.abs(this.velocity.y) < 10) return;
		if (!e.collision) return;
		this.obj.scaleX = 1.0;
		this.obj.scaleY = 1.0;
		this.animation = gs.animator.runAnim(new ScaleTo(new WrappedObject(this.obj), [1.2, 0.8], null, 3),
			function() {
				this.animation = gs.animator.runAnim(new ScaleTo(new WrappedObject(this.obj), [0.8, 1.2],
					null, 3), function() {
						this.obj.scaleX = 1.0;
						this.obj.scaleY = 1.0;
						this.animation = null;
				});
			});
	}
}
