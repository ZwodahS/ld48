package entities;

class ScorePellet extends Entity {
	var obj: h2d.Object;

	public static function make(): Entity {
		var e = new ScorePellet();
		var obj = new h2d.Object();
		obj.x = 4;
		obj.y = 4;
		var bm = Assets.packed.assets["point"].getAnim();
		bm.speed = 6;
		bm.x = -4;
		bm.y = -4;
		bm.color.setColor(Constants.ColorYellow);
		obj.addChild(bm);
		e.addChild(obj);
		e.stable = true;
		e.collision = false;
		e.obj = obj;
		return e;
	}

	override public function onCollide(e: Entity, gs: GameScreen) {
		if (e != gs.player) return;
		gs.destroyEntity(this);
		e.velocity.y = Math.increaseAbsoluteValue(e.velocity.y, 60);
	}
}
