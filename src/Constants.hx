/**
	Constants are constants value / magic numbers for the game.
	These should be set to be final.

	For the evil globals counterpart, see Globals.hx
	For the function counterpart, see Utils.hx
**/
class Constants {
	public static final Version: String = "0.2.0";

	public static final ColorBg = 0x14182e;

	public static final PlayerVelocityX = 120;
	public static final ColorWhite = 0xfff5ffe8;
	public static final ColorDarkGrey = 0xff4d494d;
	public static final ColorGrey = 0xff8696a2;
	public static final ColorGreen = 0xff63ab3f;
	public static final ColorRed = 0xffe64539;
	public static final ColorYellow = 0xffffee83;
	public static final ColorPlayer = 0xff4fa4b8;
	public static final AccelerationY = 200;
	public static final Drag = 100;
}
