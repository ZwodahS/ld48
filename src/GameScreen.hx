import hxd.Key;

import zf.animations.*;

import h2d.SpriteBatch;
import h2d.SpriteBatch.BatchElement;

class SpeedParticle {
	var b: BatchElement;
	var player: Entity;
	var speed: Float;

	public function new(b: BatchElement, player: Entity, speed: Float) {
		this.b = b;
		this.player = player;
		this.speed = speed;
	}

	public function update(dt: Float) {
		if (this.player.velocity.y < 0) return;
		this.b.y -= (this.player.velocity.y + this.speed) * dt;
		if (this.b.y < -50) this.b.y = 250;
	}
}

class GameScreen extends zf.Screen {
	static final MAX_STEP_Y = 3; // how many pixel we allow to "step up a step"
	public static var ScreenShift: Float = 180;

	public var pause: Bool = true;
	public var player: Entity;

	public var entities: Array<Entity>;
	public var instructionLayer: h2d.Layers;
	public var gameLayer: h2d.Layers;
	public var hudLayer: h2d.Layers;
	public var worldLayer: h2d.Layers;
	public var entitiesLayer: h2d.Layers;
	public var particlesLayer: h2d.Layers;
	public var speedParticlesLayer: h2d.SpriteBatch;
	public var speedParticles: Array<SpeedParticle>;
	public var r: hxd.Rand;

	public var depthText: h2d.HtmlText;
	public var timeText: h2d.HtmlText;
	public var animator: zf.animations.Animator;
	public var depth(default, set): Float = 0;
	public var time(default, set): Float = 0;
	public var state = "init";

	public var victoryLayer: h2d.Layers;
	public var victoryText: h2d.HtmlText;
	public var victoryScore: h2d.HtmlText;
	public var victoryInstruction: h2d.HtmlText;

	public function set_depth(f: Float): Float {
		this.depth = f;
		this.depthText.text = 'Depth ' + '${Std.int(this.depth)}'.font(Constants.ColorGreen);
		return this.depth;
	}

	public function set_time(f: Float): Float {
		this.time = f;
		this.timeText.text = 'Time ' + '${StringUtils.formatFloat(this.time, 2)}'.font(Constants.ColorGreen);
		return this.time;
	}

	public function new() {
		super();
		this.r = new hxd.Rand(Random.int(0, 0x7FFFFFFE));
		this.animator = new zf.animations.Animator();
		this.entities = [];
		this.addChild(this.gameLayer = new h2d.Layers());
		this.bg = new h2d.Bitmap(h2d.Tile.fromColor(Constants.ColorBg, 140, 400));
		this.gameLayer.addChild(bg);
		this.gameLayer.addChild(this.worldLayer = new h2d.Layers());
		this.worldLayer.addChild(this.entitiesLayer = new h2d.Layers());
		this.worldLayer.addChild(this.particlesLayer = new h2d.Layers());
		this.gameLayer.addChild(this.hudLayer = new h2d.Layers());
		this.hudLayer.addChild(this.depthText = new h2d.HtmlText(Assets.defaultFont2x));
		this.hudLayer.addChild(this.timeText = new h2d.HtmlText(Assets.defaultFont2x));
		this.hudLayer.addChild(this.victoryLayer = new h2d.Layers());
		this.victoryLayer.addChild(this.victoryText = new h2d.HtmlText(Assets.defaultFont3x));
		this.victoryLayer.addChild(this.victoryScore = new h2d.HtmlText(Assets.defaultFont2x));
		this.victoryLayer.addChild(this.victoryInstruction = new h2d.HtmlText(Assets.defaultFont2x));
		this.speedParticles = [];
		this.depth = 0;
		this.time = 0;
		this.depthText.setX(5).setY(5);
		this.timeText.setX(75).setY(5);

		this.victoryText.y = 80;
		this.victoryText.text = 'VICTORY';
		this.victoryText.maxWidth = 140;
		this.victoryText.textAlign = Center;

		this.victoryScore.putBelow(this.victoryText, [0, 4]);
		this.victoryScore.textAlign = Center;
		this.victoryScore.x = 0;
		this.victoryScore.maxWidth = 140;
		this.victoryScore.text = [
			'Depth: ${Std.int(this.depth)}',
			'Time: ${StringUtils.formatFloat(this.time, 2)}',
		].join("<br />");

		this.victoryInstruction.putBelow(this.victoryScore, [0, 20]);
		this.victoryInstruction.textAlign = Center;
		this.victoryInstruction.x = 0;
		this.victoryInstruction.maxWidth = 140;
		this.victoryInstruction.text = 'SPACE or TAP to restart';

		this.victoryLayer.visible = false;

		this.addChild(this.instructionLayer = new h2d.Layers());
		var box = Assets.boxFactory.make([100, 130]).centerX(0, Globals.game.boundedSize.x).setY(50);
		this.instructionLayer.addChild(box);

		var title = new h2d.HtmlText(Assets.defaultFont3x);
		title.text = "PLUNGE";
		title.putAbove(box, [0, 12]);
		title.centerX(0, 140);

		var titleShadow = new h2d.HtmlText(Assets.defaultFont3x);
		titleShadow.text = "PLUNGE".font(Constants.ColorDarkGrey);
		titleShadow.x = title.x + 1;
		titleShadow.y = title.y + 1;
		this.instructionLayer.addChild(titleShadow);
		this.instructionLayer.addChild(title);

		var byZwodahS = new h2d.HtmlText(Assets.defaultFont1x);
		byZwodahS.text = "by ZwodahS".font(Constants.ColorGrey);
		byZwodahS.putBelow(title, [0, 2]);
		byZwodahS.x = title.x + title.getSize().width - byZwodahS.getSize().width / 2;
		this.instructionLayer.addChild(byZwodahS);

		var instructionTitle = new h2d.HtmlText(Assets.defaultFont2x);
		instructionTitle.text = 'Instructions';
		instructionTitle.centerX(0, 100);
		instructionTitle.y = 10;
		box.addChild(instructionTitle);

		var instructions = new h2d.HtmlText(Assets.defaultFont1x);
		instructions.maxWidth = 100;
		instructions.text = [
			"Get to the end as fast as possible", "", 'Red'.font(Constants.ColorRed) + " Kills you",
			'Green'.font(Constants.ColorGreen) + " Makes you faster",
			"If you go fast enough, you can destroy anything", "", 'A/D Left/Right to move',
			"Or touch the left and right side of the screen if you are on Mobile", "",
			"WARNING: MAY CAUSE MOTION SICKNESS".font(Constants.ColorRed)].join("<br />");
		instructions.putBelow(instructionTitle, [0, 10]).setX(0);
		instructions.textAlign = Center;
		box.addChild(instructions);

		var i = new h2d.HtmlText(Assets.defaultFont1x);
		i.text = 'SPACE or TAP to start';
		i.y = 130 - i.getSize().y - 10;
		i.centerX(0, 100);
		box.addChild(i);
		GameScreen.ScreenShift = Globals.game.gameHeight - 110;

		this.worldLayer.alpha = 0.5;

		startNewGame();
	}

	public function startNewGame() {
		this.entitiesLayer.removeChildren();

		this.player = Player.make();
		this.player.centerX(0, Globals.game.boundedSize.x);
		this.player.y = 10;
		this.depth = 0;
		this.time = 0;

		this.worldLayer.y = 0;
		this.entities = [];
		this.entities.push(this.player);
		this.entitiesLayer.addChild(this.player);

		var x = r.random(Globals.game.boundedSize.x - 8);
		for (i in 0...1000) {
			x += 30 + r.random(70);
			x = x % (Globals.game.boundedSize.x + 7) - 7;
			var y = 200 + i * 10;
			if (r.randomChance(10)) {
				addEntity(configureEntity(SpikeBox.make(), x, y));
			} else if (r.randomChance(40)) {
				addEntity(configureEntity(ScorePellet.make(), x, y));
			} else {
				for (n in 0...8) {
					if (r.randomChance(75)) continue;
					addEntity(configureEntity(Brick.make(), x + n * 10, y));
				}
				x += 80;
			}
		}

		var exit = Exit.make();
		this.entities.push(exit);
		this.entitiesLayer.addChild(exit);
		exit.y = 10000;
	}

	public function victory() {
		this.state = "victory";
		this.victoryText.text = 'VICTORY';
		this.victoryScore.text = [
			'Time: ' + '${StringUtils.formatFloat(this.time, 2)}'.font(Constants.ColorGreen),
		].join("<br />");
		this.victoryLayer.visible = true;
		this.worldLayer.alpha = .5;
		this.clearSpeedParticles();
	}

	public function destroyEntity(e: Entity) {
		this.entities.remove(e);
		e.remove();
	}

	public function explodeEntity(e: Entity, color: Int) {
		var bm = Assets.packed.assets["white_bg"].getBitmap();
		bm.color.setColor(color);
		var explode = new Explode(bm, 8, 1.5, 48);
		this.particlesLayer.addChild(bm);
		bm.x = e.x;
		bm.y = e.y;
		this.animator.runAnim(explode);
	}

	public function addEntity(e: Entity) {
		this.entities.push(e);
		this.entitiesLayer.addChild(e);
	}

	function configureEntity(e: Entity, x: Float, y: Float): Entity {
		e.x = x;
		e.y = y;
		return e;
	}

	public function explodePlayer() {
		explodeEntity(this.player, Constants.ColorPlayer);
		this.destroyEntity(this.player);
		this.player = null;
		this.state = "gameover";
		this.victoryText.text = 'GAME OVER';
		this.victoryScore.text = [
			'Depth: ' + '${Std.int(this.depth)}'.font(Constants.ColorGreen),
			'Time: ' + '${StringUtils.formatFloat(this.time, 2)}'.font(Constants.ColorGreen),
		].join("<br />");
		this.victoryLayer.visible = true;
		this.worldLayer.alpha = .5;
		this.clearSpeedParticles();
	}

	override public function update(dt: Float) {
		if (this.pause) return;
		this.animator.update(dt);
		if (this.state != "started") return;
		this.time += dt;
		updateControl();
		for (p in this.speedParticles) {
			p.update(dt);
		}
		for (e in this.entities) {
			e.update(dt);
		}
		if (this.player == null) return;
		// we only need to test bound of everything against the player
		var playerRect = this.player.bound;
		var collisions: Array<{e: Entity, i: IntersectDetail}> = [];
		for (e in this.entities) {
			if (e == this.player) continue;
			var r = e.bound;
			if (r == null) continue;
			var intersect = playerRect.intersectDetail(r);
			if (intersect.xType == None || intersect.yType == None) continue;
			collisions.push({e: e, i: intersect});
		}

		if (collisions.length != 0) {
			for (c in collisions) {
				if (this.player == null) break;
				c.e.onCollidePlayer(this.player, c.i, this);
				if (this.player == null) break;
				this.player.onCollide(c.e, this);
				c.e.onCollide(this.player, this);
			}
		}
		if (this.player == null) return;

		var target = -(this.player.y - ScreenShift);
		if (this.player.velocity.y > 0) target -= this.player.velocity.y / 3;
		this.worldLayer.y = Math.clampF(target, this.worldLayer.y - 8, this.worldLayer.y);
		this.depth = Math.clampF(this.player.y, this.depth, null);
		if (this.player.velocity.y < 200) {
			clearSpeedParticles();
		} else {
			startSpeedParticles();
			this.speedParticlesLayer.alpha = (this.player.velocity.y - 200) / 100;
		}
	}

	function clearSpeedParticles() {
		this.speedParticlesLayer.remove();
		this.speedParticlesLayer = null;
		this.speedParticles = [];
	}

	function startSpeedParticles() {
		if (this.speedParticlesLayer != null) return;
		var tile = h2d.Tile.fromColor(0xFF000000 | Constants.ColorYellow, 1, 1);
		this.speedParticlesLayer = new h2d.SpriteBatch(tile);
		this.gameLayer.addChild(this.speedParticlesLayer);
		for (x in 0...140) {
			var y = r.random(300);
			var b = this.speedParticlesLayer.alloc(tile);
			b.x = x;
			b.y = y;
			b.alpha = .5;
			var sp = new SpeedParticle(b, this.player, 25 + r.random(50));
			this.speedParticles.push(sp);
		}
	}

	function updateControl() {
		if (this.player != null) {
			if (isMoveLeft()) {
				this.player.velocity.x = -Constants.PlayerVelocityX;
			} else if (isMoveRight()) {
				this.player.velocity.x = Constants.PlayerVelocityX;
			} else {
				this.player.velocity.x = 0;
			}
		}
	}

	var touchDown: String = "";

	function isMoveLeft(): Bool {
		if (Key.isDown(Key.A) || Key.isDown(Key.LEFT)) return true;
		if (touchDown == "left") return true;
		return false;
	}

	function isMoveRight(): Bool {
		if (Key.isDown(Key.D) || Key.isDown(Key.RIGHT)) return true;
		if (touchDown == "right") return true;
		return false;
	}

	override public function render(engine: h3d.Engine) {}

	override public function onEvent(event: hxd.Event) {
		if (this.state == "init" || this.state == "gameover" || this.state == "victory") {
			if (event.kind == EKeyDown && event.keyCode == Key.SPACE || event.kind == ERelease) {
				start();
			}
		} else if (this.state == "started") {
			if (event.kind == EKeyDown && event.keyCode == Key.SPACE) {
#if debug
				this.pause = !this.pause;
#end
			} else if (event.kind == EPush) {
				if (event.relX < Globals.game.gameWidth / 2) {
					this.touchDown = "left";
				} else {
					this.touchDown = "right";
				}
			} else if (event.kind == ERelease) {
				this.touchDown = "";
			}
		}
	}

	public function start() {
		if (this.state == "gameover" || this.state == "victory") startNewGame();
		this.pause = false;
		this.worldLayer.alpha = 1.0;
		this.instructionLayer.visible = false;
		this.state = "started";
		this.victoryLayer.visible = false;
	}

	override public function destroy() {}

	var bg: h2d.Object;

	override public function resize(width: Float, height: Float) {
		GameScreen.ScreenShift = height - 130;
	}
}
