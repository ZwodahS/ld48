/**
	Assets is used to store loaded assets
**/

import zf.Assets.LoadedSpritesheet;
import zf.ui.TileBoxFactory;

class Assets {
	public static var packed: LoadedSpritesheet;
	public static var boxFactory: TileBoxFactory;

	public static var fontZP10x10: hxd.res.BitmapFont;
	public static var defaultFont1x: h2d.Font;
	public static var defaultFont2x: h2d.Font;
	public static var defaultFont3x: h2d.Font;
}
